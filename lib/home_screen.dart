import 'package:flutter/material.dart';
import 'package:widgets_app/screens/bottomnavigation.dart';

import 'package:widgets_app/screens/new_column.dart';
import 'package:widgets_app/screens/padding.dart';
import 'package:widgets_app/screens/pageview.dart';
import 'package:widgets_app/screens/theme.dart';
import 'screens/alertdialog.dart';
import 'screens/alignmentcontainer.dart';
import 'screens/animatedalign.dart';
import 'screens/autocomplete.dart';
import 'screens/bottomsheet.dart';
import 'screens/checkbox.dart';
import 'screens/customscrollview.dart';
import 'screens/datatable.dart';
import 'screens/datatable2.dart';
import 'screens/datepicker.dart';
import 'screens/dismissible.dart';
import 'screens/divider.dart';
import 'screens/draggable.dart';
import 'screens/dragabble.dart';
import 'screens/drawer.dart';
import 'screens/dropdown.dart';
import 'screens/elevated.dart';
import 'screens/expanded_new_screen.dart';
import 'screens/expandedpanel.dart';
import 'screens/fittedbox.dart';
import 'screens/floating.dart';
import 'screens/form.dart';
import 'screens/futurebuilder.dart';
import 'screens/gesturedetector.dart';
import 'screens/hero.dart';
import 'screens/ignorpointer.dart';
import 'screens/inkwell_new_2.dart';
import 'screens/inkwellnew.dart';
import 'screens/interactive.dart';
import 'screens/lob.dart';
import 'screens/pop_new_menu.dart';
import 'screens/slivergrid.dart';
import 'screens/stack.dart';
import 'screens/gridview.dart';
import 'screens/icons.dart';
import 'screens/layout.dart';
import 'screens/listtile.dart';
import 'screens/listview.dart';
import 'screens/radiobutton.dart';
import 'screens/reorderlistview.dart';
import 'screens/scrollbar.dart';
import 'screens/slider.dart';
import 'screens/sliverappbar.dart';
import 'screens/streambuilder.dart';
import 'screens/styling_main.dart';
import 'screens/appbar.dart';
import 'screens/container.dart';
import 'screens/tabbar.dart';
import 'screens/table.dart';
import 'screens/textbutton.dart';
import 'screens/textfield.dart';
import 'screens/transformation.dart';

class HomeScreen extends StatefulWidget {
  @override
  HomeScreenState createState() => HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Widgets App',
          style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
        ),
        backgroundColor: Colors.red,
      ),
      drawer: Drawer(),
      body: ListView(
        padding: EdgeInsets.only(top: 20, left: 60, right: 60),
        children: [
          ElevatedButton(
            child: Text(
              'App Bar',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => AppBarscreen(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'Column',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => NewColumn(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'Container',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => BoxScreen(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'Media Query',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => StylingTwo(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'Padding',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => PagePadding(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'Theme',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ThemePage(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'ScrollBar',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ScrollbarScreen(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'ReOrdered ListView',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ReorderList(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'PageView',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => PageviewScreen(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'ListView',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ListScreen(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'GridView ',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => GridScreen(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'DragllabeScrollableSheet',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => DragScrollScreen(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'CustomScrollView',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => CustomScrollScreen(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'BottomNavigationBar',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => BottomScreen()),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'Drawer',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => DrawerScreen()),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'SliverAppbar',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => SliverScreen()),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'TabBar',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => TabBarScreen()),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'DropDownMenu Button',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => DropDownScreen()),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'Elevated Button',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => ElevatedScreen()),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'Floating Action Button',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => FabScreen(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'IconButton',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => IconButtonScreen(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'RadioButton',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => RadioButtonScreen(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'TextButton',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => TextButtonScreen(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'CheckBox',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => CheckBoxScreen(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'Date Picker',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => DatePickerScreen(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'Slider Widget',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => SliderWidget(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'TextField Widget',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => TextFieldScreen(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'AlertDialog',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => AlertDialogScreen(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'ModelBottomSheet',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => BottomSheetScreen(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'ExpandedPanel List',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ExpandedPanelScreen(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'DataTable',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => DataTableScreen(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'DataTable 2',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => DataTable2Screen(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'Divider',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => DividerScreen(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'ListTile',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ListTileScreen(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'FittedBox',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => FittedBoxScreen(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'Transformation',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => TransformationScreen(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'LayoutBuilder',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => LayoutBuilderScreen(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'Stack',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => StackScreen(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'Table',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => TableScreen(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'SliverGrid',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => SliverGridScreen(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'Dismissible',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => DismissibleScreen(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'Draggable',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => DragabbleScreen(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'GestureDetector',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => GestureDetectorScreen(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'Ignore Pointer',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => IgnorePointerScreen(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'Interactive Viewer',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => InteractiveScreen(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'Hero Widget',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => HeroScreen(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'AutoComplete TextField',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => AutoCompleteField(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'Form Widget',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => FormFieldScreen(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'StreamBuilder Widget',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => StreamBuilderScreen(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'FutureBuilder Widget',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => FutureBuilderScreen(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'AnimatedAlign Widget',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => AnimatedAlignScreeen(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'Alignment Container',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => AlignmentContainerScreen(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'InkWell',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => InkWellNewScreen(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'InkWell 2',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => InkwellNew2Screen(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'Expanded',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ExpandedNewScreen(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'PopupMenu',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => PopupMenuScreen(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'L O B',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => Lob(),
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
