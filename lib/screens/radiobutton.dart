import 'package:flutter/material.dart';

class RadioButtonScreen extends StatefulWidget {
  @override
  _RadioButtonScreenState createState() => _RadioButtonScreenState();
}

enum SingingCharacter { TechyPanther, IgnekInfotech }

class _RadioButtonScreenState extends State<RadioButtonScreen> {
  SingingCharacter character = SingingCharacter.TechyPanther;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('RadioButton Widget '),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ListTile(
              title: Text('TechyPanther'),
              leading: Radio(
                value: SingingCharacter.TechyPanther,
                groupValue: character,
                activeColor: Colors.red,
                toggleable: true,
                onChanged: (SingingCharacter value) {
                  setState(() {
                    character = value;
                  });
                },
              ),
            ),
            ListTile(
              title: Text('IgnekInfotech'),
              leading: Radio(
                value: SingingCharacter.IgnekInfotech,
                groupValue: character,
                activeColor: Colors.green,
                onChanged: (SingingCharacter value) {
                  setState(() {
                    character = value;
                  });
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
