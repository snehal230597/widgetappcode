import 'package:flutter/material.dart';

class TabBarScreen extends StatefulWidget {
  @override
  _TabBarScreenState createState() => _TabBarScreenState();
}

class _TabBarScreenState extends State<TabBarScreen> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: 1,
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          title: Text('TabBar Widget'),
          bottom: TabBar(
            indicatorColor: Colors.black,
            labelColor: Colors.black,
            tabs: [
              Tab(
                icon: Icon(Icons.cloud_outlined),
              ),
              Tab(
                icon: Icon(Icons.beach_access_sharp),
              ),
              Tab(
                icon: Icon(Icons.brightness_5_sharp),
              )
            ],
          ),
        ),
        body: TabBarView(
          children: [
            Center(
              child: Text('Its Cloudy here!', textScaleFactor: 3),
            ),
            Center(
              child: Text('Its Rainy here!', textScaleFactor: 3),
            ),
            Center(
              child: Text('Its Sunny here!', textScaleFactor: 3),
            )
          ],
        ),
      ),
    );
  }
}
