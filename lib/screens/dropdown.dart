import 'package:flutter/material.dart';

class DropDownScreen extends StatefulWidget {
  @override
  _DropDownScreenState createState() => _DropDownScreenState();
}

class _DropDownScreenState extends State<DropDownScreen> {
  String dropDownvalue = 'One';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('DropdwonMenu Button'),
      ),
      body: Center(
        child: DropdownButton(
          iconSize: 30,
          value: dropDownvalue,
          icon: Icon(Icons.arrow_downward),
          elevation: 16,
          underline: Container(
            height: 2,
            color: Colors.black,
          ),
          onChanged: (String newValue) {
            setState(
              () {
                dropDownvalue = newValue;
              },
            );
          },
          items: ['One', 'Two', 'Three', 'Four'].map((String value) {
            return DropdownMenuItem(
              value: value,
              child: Text(value),
            );
          }).toList(),
        ),
      ),
    );
  }
}
