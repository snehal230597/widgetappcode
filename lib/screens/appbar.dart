import 'package:flutter/material.dart';

class AppBarscreen extends StatefulWidget {
  @override
  _AppBarscreenState createState() => _AppBarscreenState();
}

class _AppBarscreenState extends State<AppBarscreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 70,
        //titleTextStyle: TextStyle(fontSize: 10),
        titleSpacing: 20,
        shadowColor: Colors.green,
        backgroundColor: Colors.red,
        elevation: 10,
        title: Text(
          'AppBar Widget',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),

        actions: [
          TextButton(
            onPressed: () {
              print('Action Button Clicked');
            },
            child: Text(
              'Action 1',
              style: TextStyle(color: Colors.white),
            ),
          ),
          IconButton(
            icon: Icon(Icons.add_alert),
            onPressed: () {
              setState(
                () {
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: Text('This Is A SnackBar'),
                    ),
                  );
                },
              );
            },
            tooltip: 'ShowSnakeBar',
          ),
        ],
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            FlutterLogo(
              size: 200,
              style: FlutterLogoStyle.stacked,
              textColor: Colors.black,
            ),
            ElevatedButton(
              child: Text('Closed AppBar'),
              onPressed: () {
                setState(
                  () {
                    Navigator.of(context).pop();
                  },
                );
              },
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        color: Colors.green,
        elevation: 12,
        child: Container(
          height: 60,
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        backgroundColor: Colors.red,
        tooltip: 'FAB Long Presses',
        elevation: 10,
        onPressed: () {
          print('FAB Cliked');
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      drawer: Drawer(
        elevation: 10,
        child: DrawerHeader(
          margin: EdgeInsets.only(left: 100, right: 50, top: 20),
          child: Text(
            'Hello',
            textScaleFactor: 2,
          ),
        ),
      ),
    );
  }
}
