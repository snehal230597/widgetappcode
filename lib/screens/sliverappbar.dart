import 'package:flutter/material.dart';

class SliverScreen extends StatelessWidget {
  final bool _pinned = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.vertical(
                bottom: Radius.circular(30),
              ),
            ),
            elevation: 10,
            pinned: _pinned,
            backgroundColor: Colors.lime,
            expandedHeight: 160,
            flexibleSpace: FlexibleSpaceBar(
              title: Text(
                'SliverApp Bar',
                style: TextStyle(color: Colors.black),
              ),
              background: FlutterLogo(
                style: FlutterLogoStyle.horizontal,
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: SizedBox(
              height: 20,
              child:
                  Center(child: Text('Scroll To see the Sliver Appbar effect')),
            ),
          ),
          SliverList(
            delegate: SliverChildBuilderDelegate(
              (BuildContext context, int index) {
                return Container(
                  height: 100,
                  color: index.isOdd ? Colors.white : Colors.black12,
                  child: Center(
                    child: Text(
                      '$index',
                      textScaleFactor: 5,
                    ),
                  ),
                );
              },
              childCount: 20,
            ),
          ),
        ],
      ),
    );
  }
}
