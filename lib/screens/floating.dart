import 'package:flutter/material.dart';

class FabScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Floating Action Button'),
      ),
      body: Center(
        child: Text(
          'Press the button below',
          textScaleFactor: 2,
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {},
        label: Text('Approved'),
        backgroundColor: Colors.pink,
        icon: Icon(Icons.thumb_up),
      ),
    );
  }
}
