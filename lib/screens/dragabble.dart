import 'package:flutter/material.dart';

class DragScrollScreen extends StatefulWidget {
  @override
  _DragScrollScreenState createState() => _DragScrollScreenState();
}

class _DragScrollScreenState extends State<DragScrollScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('DragableScrollableScreeen'),
      ),
      body: SizedBox.expand(
        child: DraggableScrollableSheet(
          builder: (BuildContext context, scrollControllar) {
            return Container(
              color: Colors.blue[100],
              child: ListView.builder(
                controller: ScrollController(),
                itemCount: 25,
                itemBuilder: (BuildContext context, int index) {
                  return ListTile(
                    title: Text('Item $index'),
                  );
                },
              ),
            );
          },
        ),
      ),
    );
  }
}
