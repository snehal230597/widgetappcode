import 'package:flutter/material.dart';

class DragabbleScreen extends StatefulWidget {
  @override
  _DragabbleScreenState createState() => _DragabbleScreenState();
}

class _DragabbleScreenState extends State<DragabbleScreen> {
  int acceptedData = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Dragabble Widget Example'),
      ),
      body: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Draggable<int>(
              // Data is the value this Draggable stores.
              data: 10,
              feedback: Container(
                color: Colors.deepOrange,
                height: 150,
                width: 150,
                child: const Icon(Icons.directions_run),
              ),
              childWhenDragging: Container(
                height: 150.0,
                width: 150.0,
                color: Colors.pinkAccent,
                child: const Center(
                  child: Text('Child When Dragging'),
                ),
              ),
              child: Container(
                height: 150.0,
                width: 150.0,
                color: Colors.lightGreenAccent,
                child: const Center(
                  child: Text('Draggable'),
                ),
              ),
            ),
            DragTarget<int>(
              builder: (
                BuildContext context,
                List<dynamic> accepted,
                List<dynamic> rejected,
              ) {
                return Container(
                  height: 150.0,
                  width: 150.0,
                  color: Colors.cyan,
                  child: Center(
                    child: Text('Value is updated to: $acceptedData'),
                  ),
                );
              },
              onAccept: (int data) {
                setState(() {
                  acceptedData += data;
                });
              },
            ),
          ],
        ),
      ),
    );
  }
}
