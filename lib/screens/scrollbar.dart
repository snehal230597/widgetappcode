import 'package:flutter/material.dart';

class ScrollbarScreen extends StatefulWidget {
  @override
  _ScrollbarScreenState createState() => _ScrollbarScreenState();
}

class _ScrollbarScreenState extends State<ScrollbarScreen> {
  ScrollController firstControllar = ScrollController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ScrollBar'),
      ),
      body: LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
          return Row(
            children: [
              SizedBox(
                width: constraints.maxWidth / 2,
                child: Scrollbar(
                  isAlwaysShown: true,
                  child: ListView.builder(
                    controller: firstControllar,
                    itemCount: 100,
                    itemBuilder: (BuildContext context, int index) {
                      return Padding(
                        padding: const EdgeInsets.all(14),
                        child: Text('Scrollable 1 : Index $index'),
                      );
                    },
                  ),
                ),
              ),
              SizedBox(
                width: constraints.maxWidth / 2,
                child: Scrollbar(
                  child: ListView.builder(
                    itemCount: 100,
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        height: 50,
                        color: index.isEven
                            ? Colors.amberAccent
                            : Colors.blueAccent,
                        child: Padding(
                          padding: const EdgeInsets.all(14),
                          child: Text('Scrollable 2 : Index $index'),
                        ),
                      );
                    },
                  ),
                ),
              )
            ],
          );
        },
      ),
    );
  }
}
