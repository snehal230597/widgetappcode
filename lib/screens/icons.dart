import 'package:flutter/material.dart';

class IconButtonScreen extends StatefulWidget {
  @override
  _IconButtonScreenState createState() => _IconButtonScreenState();
}

class _IconButtonScreenState extends State<IconButtonScreen> {
  double volumn = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Icon Button'),
      ),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            IconButton(
              iconSize: 60,
              splashColor: Colors.green,
              color: Colors.pink,
              icon: Icon(Icons.volume_up),
              tooltip: 'iconbutton pressed',
              onPressed: () {
                setState(
                  () {
                    volumn += 10;
                  },
                );
              },
            ),
            Text('Volumn : $volumn')
          ],
        ),
      ),
    );
  }
}
