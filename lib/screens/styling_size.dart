import 'package:flutter/material.dart';

class Styling extends StatefulWidget {
  @override
  _StylingState createState() => _StylingState();
}

class _StylingState extends State<Styling> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Media-Query'),
      ),
      body: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            color: Colors.red,
            width: MediaQuery.of(context).size.width * 0.3,
            height: MediaQuery.of(context).size.height * 0.5,
          ),
          Container(
            color: Colors.blue,
            width: MediaQuery.of(context).size.width * 0.3,
          ),
        ],
      ),
    );
  }
}
