import 'package:flutter/material.dart';

class StackScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Stack Widget'),
      ),
      body: ListView(
        padding: EdgeInsets.only(top: 20),
        children: [
          Stack(
            alignment: Alignment.center,
            children: [
              Container(
                height: 200,
                width: 200,
                color: Colors.red,
              ),
              Container(
                height: 180,
                width: 180,
                color: Colors.red,
              ),
              Container(
                height: 160,
                width: 160,
                color: Colors.yellow,
              ),
              Container(
                height: 140,
                width: 140,
                color: Colors.green,
              ),
              Container(
                height: 120,
                width: 120,
                color: Colors.brown,
              ),
              Container(
                height: 100,
                width: 100,
                color: Colors.pink,
              ),
              FlutterLogo(
                size: 70,
              )
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Center(
            child: SizedBox(
              width: 200,
              height: 200,
              child: Stack(
                children: [
                  Container(
                    height: 200,
                    width: 200,
                    color: Colors.lime,
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    alignment: Alignment.bottomCenter,
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [Colors.red, Colors.yellow, Colors.black])),
                    child: Center(
                      child: Text(
                        'ForeGround Text',
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 20),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          SizedBox(height: 20),
          Center(child: Text("Wrap Widget")),
          Wrap(
            spacing: 10,
            runSpacing: 2,
            children: <Widget>[
              Chip(
                avatar: CircleAvatar(
                    backgroundColor: Colors.blue.shade900,
                    child: const Text('A')),
                label: const Text('Hamilton'),
              ),
              Chip(
                avatar: CircleAvatar(
                    backgroundColor: Colors.blue.shade900,
                    child: const Text('M')),
                label: const Text('Lafayette'),
              ),
              Chip(
                avatar: CircleAvatar(
                    backgroundColor: Colors.blue.shade900,
                    child: const Text('H')),
                label: const Text('Mulligan'),
              ),
              Chip(
                avatar: CircleAvatar(
                    backgroundColor: Colors.blue.shade900,
                    child: const Text('L')),
                label: const Text('Laurens'),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
