import 'package:flutter/material.dart';

class AutoCompleteField extends StatefulWidget {
  @override
  _AutoCompleteFieldState createState() => _AutoCompleteFieldState();
}

class _AutoCompleteFieldState extends State<AutoCompleteField> {
  List<String> newOption = <String>['ayush', 'boby', 'cody', 'denny', 'ellina'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('AutoComplete TextField'),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Autocomplete<String>(
            optionsBuilder: (TextEditingValue textEditingValue) {
              if (textEditingValue.text == '') {
                return const Iterable.empty();
              }
              return newOption.where((String option) {
                return option.contains(textEditingValue.text.toLowerCase());
              });
            },
            onSelected: (String selection) {
              debugPrint('You just selected $selection');
            },
          ),
        ),
      ),
    );
  }
}
