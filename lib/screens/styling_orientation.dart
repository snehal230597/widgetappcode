import 'package:flutter/material.dart';

class Orient extends StatelessWidget {
  Widget potrait() {
    return Center(
      child: Container(
        alignment: Alignment.center,
        height: 100,
        width: 300,
        color: Colors.amber,
        child: Text(
          'Portrait',
          textScaleFactor: 2,
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
    );
  }

  Widget landscape() {
    return Center(
      child: Container(
        alignment: Alignment.center,
        height: 100,
        width: 300,
        color: Colors.amber,
        child: Text(
          'Landscape',
          textScaleFactor: 2,
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Orientation'),
      ),
      body: OrientationBuilder(
        builder: (context, orientation) {
          if (orientation == Orientation.portrait) {
            return potrait();
          } else {
            return landscape();
          }
        },
      ),
    );
  }
}
