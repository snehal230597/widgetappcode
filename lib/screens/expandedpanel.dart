import 'dart:ui';

import 'package:flutter/material.dart';

class ExpandedPanelScreen extends StatefulWidget {
  @override
  _ExpandedPanelScreenState createState() => _ExpandedPanelScreenState();
}

class _ExpandedPanelScreenState extends State<ExpandedPanelScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Expendaed Panel List'),
      ),
      body: Center(
        child: Column(
          children: [
            ExpansionPanelList(
              expansionCallback: (int index, bool isExpanded) {},
              children: [
                ExpansionPanel(
                  headerBuilder: (BuildContext context, isExpanded) {
                    return ListTile(
                      title: Text('Item 1'),
                    );
                  },
                  body: ListTile(
                    title: Text('Item 1 child'),
                    subtitle: Text('Details goes here'),
                  ),
                  isExpanded: true,
                ),
                ExpansionPanel(
                  headerBuilder: (BuildContext context, isExpanded) {
                    return ListTile(
                      title: Text('Item 2'),
                    );
                  },
                  body: ListTile(
                    title: Text('Item 2 child'),
                    subtitle: Text('Details goes here'),
                  ),
                  isExpanded: false,
                ),
              ],
            ),
            SizedBox(height: 40),
            ElevatedButton(
                onPressed: () {
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      backgroundColor: Colors.green,
                      elevation: 10,
                      content: Text('Awesome Snackbar!'),
                      action: SnackBarAction(
                          label: 'Action',
                          textColor: Colors.black,
                          onPressed: () {}),
                    ),
                  );
                },
                child: Text(
                  'Show SnackBar',
                  textScaleFactor: 1.5,
                  style: TextStyle(color: Colors.black),
                ))
          ],
        ),
      ),
    );
  }
}
