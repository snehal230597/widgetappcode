import 'package:flutter/material.dart';

class DataTableScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('DataTable'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            SizedBox(height: 20),
            Tooltip(
              message: 'I Am Tooltip',
              decoration: BoxDecoration(
                  color: Colors.pink, borderRadius: BorderRadius.circular(30)),
              child: Text(
                'Basic DataTable',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
              ),
            ),
            SizedBox(height: 20),
            DataTable(
                columnSpacing: 70,
                dataRowHeight: 50,
                headingRowColor:
                    MaterialStateProperty.resolveWith((states) => Colors.lime),
                decoration: BoxDecoration(border: Border.all()),
                columns: [
                  DataColumn(label: Text('Name')),
                  DataColumn(label: Text('Age')),
                  DataColumn(label: Text('Role')),
                ],
                rows: [
                  DataRow(
                    cells: [
                      DataCell(Text('Jenni')),
                      DataCell(Text('21')),
                      DataCell(Text('Student')),
                    ],
                  ),
                  DataRow(
                    cells: [
                      DataCell(Text('joya')),
                      DataCell(Text('25')),
                      DataCell(Text('Professor')),
                    ],
                  ),
                  DataRow(
                    cells: [
                      DataCell(Text('Sarah')),
                      DataCell(Text('30')),
                      DataCell(Text('Assistant Professer')),
                    ],
                  ),
                  DataRow(
                    cells: [
                      DataCell(Text('Jenni')),
                      DataCell(Text('21')),
                      DataCell(Text('Student')),
                    ],
                  ),
                  DataRow(
                    cells: [
                      DataCell(Text('joya')),
                      DataCell(Text('25')),
                      DataCell(Text('Professor')),
                    ],
                  ),
                  DataRow(
                    cells: [
                      DataCell(Text('Sarah')),
                      DataCell(Text('30')),
                      DataCell(Text('Assistant Professer')),
                    ],
                  ),
                ]),
          ],
        ),
      ),
    );
  }
}
