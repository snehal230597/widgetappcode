import 'package:flutter/material.dart';

class BottomSheetScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ModalBottomSheet'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Circular Progress Indicator'),
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.only(top: 20),
              child: CircularProgressIndicator(
                strokeWidth: 8,
                backgroundColor: Colors.pink,
              ),
            ),
            SizedBox(height: 20),
            Text('Linear Progress Indicator'),
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.only(top: 20),
              child: LinearProgressIndicator(
                minHeight: 10,
                backgroundColor: Colors.pink,
              ),
            ),
            SizedBox(height: 20),
            Text(
              'Chip',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            Chip(
              label: Text('Hello Buddies'),
              backgroundColor: Colors.pinkAccent,
              elevation: 15,
              shadowColor: Colors.green,
              avatar: CircleAvatar(
                backgroundColor: Colors.lightGreen,
                child: Text('A'),
              ),
            ),
            SizedBox(height: 10),
            Text('Card Widget',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
            SizedBox(height: 20),
            Card(
              child: Column(
                children: [
                  ListTile(
                    leading: Icon(Icons.album),
                    title: Text('The Enchanted Nightingale'),
                    subtitle:
                        Text('Music by Julie Gable. Lyrics by Sidney Stein.'),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      TextButton(
                        child: const Text('BUY TICKETS'),
                        onPressed: () {},
                      ),
                      const SizedBox(width: 8),
                      TextButton(
                        child: const Text('LISTEN'),
                        onPressed: () {},
                      ),
                      const SizedBox(width: 8),
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(height: 20),
            ElevatedButton(
                onPressed: () {
                  showModalBottomSheet(
                    context: context,
                    elevation: 10,
                    builder: (context) {
                      return Wrap(
                        children: [
                          ListTile(
                            leading: Icon(Icons.share),
                            title: Text('Share'),
                            onTap: () {},
                          ),
                          ListTile(
                            leading: Icon(Icons.copy),
                            title: Text('Copy Link'),
                            onTap: () {},
                          ),
                          ListTile(
                            leading: Icon(Icons.edit),
                            title: Text('Edit'),
                            onTap: () {},
                          ),
                        ],
                      );
                    },
                  );
                },
                child: Text('Show ModalBottomSheet')),
          ],
        ),
      ),
    );
  }
}
