import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class InkWellNewScreen extends StatefulWidget {
  @override
  _InkWellNewScreenState createState() => _InkWellNewScreenState();
}

class _InkWellNewScreenState extends State<InkWellNewScreen> {
  String inkwell = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            InkWell(
              onTap: () {
                setState(() {
                  inkwell = 'Inkwell Tapped';
                });
              },
              onDoubleTap: () {
                setState(() {
                  inkwell = 'Inkwell DoubleTapped';
                });
              },
              onLongPress: () {
                setState(() {
                  inkwell = 'InkWell Long Pressed';
                });
              },
              borderRadius: BorderRadius.circular(20),
              child: Container(
                  color: Colors.green,
                  width: 120,
                  height: 70,
                  child: Center(
                      child: Text(
                    'Inkwell',
                    textScaleFactor: 2,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ))),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                inkwell,
                textScaleFactor: 2,
              ),
            )
          ],
        ),
      ),
    );
  }
}
