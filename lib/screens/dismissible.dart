import 'package:flutter/material.dart';

class DismissibleScreen extends StatefulWidget {
  @override
  _DismissibleScreenState createState() => _DismissibleScreenState();
}

class _DismissibleScreenState extends State<DismissibleScreen> {
  List<int> item = List<int>.generate(100, (int index) => index);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Dismissible'),
      ),
      body: Center(
        child: ListView.builder(
            padding: EdgeInsets.all(20),
            itemCount: item.length,
            itemBuilder: (BuildContext context, int index) {
              return Dismissible(
                background: Container(
                  color: Colors.green,
                ),
                key: ValueKey(item[index]),
                onDismissed: (DismissDirection direction) {
                  setState(() {
                    item.removeAt(index);
                  });
                },
                child: Text(
                  'Item ${item[index]}',
                  textScaleFactor: 2,
                ),
              );
            }),
      ),
    );
  }
}
