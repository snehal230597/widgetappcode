import 'package:flutter/material.dart';

class IgnorePointerScreen extends StatefulWidget {
  @override
  _IgnorePointerScreenState createState() => _IgnorePointerScreenState();
}

class _IgnorePointerScreenState extends State<IgnorePointerScreen> {
  bool ignoring = false;
  void setIgnoring(bool newValue) {
    setState(() {
      ignoring = newValue;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: ElevatedButton(
            onPressed: () {
              setIgnoring(!ignoring);
            },
            child: Text(
              ignoring ? 'Set ignoring to false' : 'Set ignoring to true',
            ),
          ),
        ),
      ),
      body: IgnorePointer(
        ignoring: ignoring,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Text('Ignornig : $ignoring'),
              ElevatedButton(
                onPressed: () {},
                child: Text('Click Me'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
