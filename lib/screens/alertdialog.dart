import 'package:flutter/material.dart';

class AlertDialogScreen extends StatefulWidget {
  @override
  _AlertDialogScreenState createState() => _AlertDialogScreenState();
}

class _AlertDialogScreenState extends State<AlertDialogScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('AlertDialog'),
      ),
      body: Center(
        child: TextButton(
            onPressed: () {
              return showDialog(
                barrierColor: Colors.lime,
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    elevation: 10,
                    backgroundColor: Colors.orangeAccent,
                    title: Text('Alert Dialog Title'),
                    content: Text('Alert Dialog Discription'),
                    actions: [
                      TextButton(
                        onPressed: () {
                          return Navigator.pop(context, 'Cancel');
                        },
                        child: Text('Cancel',
                            style: TextStyle(color: Colors.black)),
                      ),
                      TextButton(
                        onPressed: () {
                          return Navigator.pop(context, 'Ok');
                        },
                        child: Text(
                          'Ok',
                          style: TextStyle(color: Colors.black),
                        ),
                      ),
                    ],
                  );
                },
              );
            },
            child: Text('Show Dialog')),
      ),
    );
  }
}
