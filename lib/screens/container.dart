import 'package:flutter/material.dart';

class BoxScreen extends StatefulWidget {
  @override
  _ContainerScreenState createState() => _ContainerScreenState();
}

class _ContainerScreenState extends State<BoxScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Container Widget',
          textScaleFactor: 1.5,
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: Colors.yellow,
      ),
      body: Center(
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.only(top: 18, left: 15),
              margin: EdgeInsets.only(top: 20),
              height: 70,
              width: 200,
              decoration: BoxDecoration(
                  border: Border.all(
                    width: 2,
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.green,
                      blurRadius: 4,
                      offset: Offset(4, 8),
                    ),
                  ],
                  borderRadius: BorderRadius.circular(15),
                  color: Colors.deepOrange),
              child: Text(
                'Hello Flutter',
                textScaleFactor: 2,
              ),
            ),
            SizedBox(height: 10),
            Container(
              height: 100,
              width: 100,
              alignment: Alignment.center,
              child: Text(
                'Hi',
                textScaleFactor: 2,
              ),
              decoration: BoxDecoration(
                border: Border.all(width: 2),
                shape: BoxShape.circle,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black,
                    blurRadius: 4,
                    offset: Offset(4, 8),
                  ),
                ],
                color: Colors.yellow,
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 18, left: 15),
              height: 70,
              width: 200,
              margin: EdgeInsets.only(top: 20),
              decoration: BoxDecoration(
                  border: Border.all(width: 2),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.deepOrange,
                      blurRadius: 4,
                      offset: Offset(4, 8),
                    )
                  ],
                  color: Colors.green),
              child: Text(
                'Hello Flutter',
                style: TextStyle(fontWeight: FontWeight.bold),
                textScaleFactor: 2,
              ),
            ),
            SizedBox(height: 10),
            Container(
              height: 100,
              width: 100,
              alignment: Alignment.center,
              child: Text(
                'Hi',
                textScaleFactor: 2,
              ),
              decoration: BoxDecoration(
                border: Border.all(width: 2),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black,
                    blurRadius: 4,
                    offset: Offset(4, 8),
                  ),
                ],
                shape: BoxShape.circle,
                color: Colors.yellow,
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 18, left: 15),
              height: 70,
              width: 200,
              margin: EdgeInsets.only(top: 15),
              decoration: BoxDecoration(
                  border: Border.all(width: 2),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.green,
                      blurRadius: 4,
                      offset: Offset(4, 8), // Shadow position
                    ),
                  ],
                  color: Colors.deepOrange,
                  borderRadius: BorderRadius.circular(15)),
              child: Text(
                'Hello Flutter',
                textScaleFactor: 2,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
