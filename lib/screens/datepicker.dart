import 'package:flutter/material.dart';

class DatePickerScreen extends StatefulWidget {
  @override
  _DatePickerScreenState createState() => _DatePickerScreenState();
}

class _DatePickerScreenState extends State<DatePickerScreen> {
  DateTime selectedDate = DateTime.now();

  selectDate() async {
    DateTime selected = await showDatePicker(
      context: context,
      initialDate: selectedDate,
      firstDate: DateTime(1997),
      lastDate: DateTime(2025),
    );
    if (selected != null && selected != selectedDate)
      setState(
        () {
          selectedDate = selected;
        },
      );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Date Picker'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            RaisedButton(
              onPressed: () {
                setState(() {
                  selectDate();
                });
              },
              color: Colors.lightBlue,
              child: Text(
                'Chooes Date',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            SizedBox(height: 20),
            Text(
                '${selectedDate.day}/${selectedDate.month}/${selectedDate.year}'),
          ],
        ),
      ),
    );
  }
}
