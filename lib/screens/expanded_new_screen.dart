import 'package:flutter/material.dart';

class ExpandedNewScreen extends StatefulWidget {
  @override
  _ExpandedNewScreenState createState() => _ExpandedNewScreenState();
}

class _ExpandedNewScreenState extends State<ExpandedNewScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Row(
          children: [
            Flexible(
              child: Container(
                height: 100,
                // width: 100,
                color: Colors.yellow,
              ),
            ),
            Container(
              height: 100,
              width: 50,
              color: Colors.pink,
            ),
            Flexible(
              child: Container(
                height: 100,
                //  width: 100,
                color: Colors.green,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
