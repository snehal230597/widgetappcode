import 'package:flutter/material.dart';

class ListTileScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ListTile Widget'),
      ),
      body: Column(
        children: [
          Container(
            height: 200,
            width: 200,
            color: Colors.pink,
            child: Baseline(
              baseline: 100,
              baselineType: TextBaseline.alphabetic,
              child: Container(
                height: 50,
                width: 50,
                color: Colors.green,
              ),
            ),
          ),
          SizedBox(height: 20),
          Center(
            child: Padding(
              padding: const EdgeInsets.only(left: 8, right: 8),
              child: Card(
                color: Colors.lime,
                elevation: 15,
                child: Tooltip(
                  message: 'This is A ListTile Widget',
                  child: ListTile(
                    leading: Icon(
                      Icons.ac_unit_outlined,
                      color: Colors.redAccent,
                    ),
                    onTap: () {
                      debugPrint('hi');
                    },
                    title: Text('Flutter',
                        style: TextStyle(fontWeight: FontWeight.bold)),
                    subtitle: Text('flutter uses a DART Language!!!'),
                    trailing: Icon(
                      Icons.add_a_photo_sharp,
                      color: Colors.pink,
                    ),
                  ),
                ),
              ),
            ),
          ),
          SizedBox(height: 10),
          Text(
            'ConstrainedBox',
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          ConstrainedBox(
            constraints: BoxConstraints.expand(height: 100, width: 300),
            child: Container(
              padding: EdgeInsets.only(left: 10),
              color: Colors.amber,
              child: Center(
                child: Text(
                    'A Computer Science portal for geeks. It contains gd rgr rgdrht fdghgj grth fhhg gjftyh fctyh xytrf drygh '),
              ),
            ),
          )
        ],
      ),
    );
  }
}
