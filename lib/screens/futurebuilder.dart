import 'package:flutter/material.dart';

class FutureBuilderScreen extends StatefulWidget {
  @override
  _FutureBuilderScreeenState createState() => _FutureBuilderScreeenState();
}

class _FutureBuilderScreeenState extends State<FutureBuilderScreen> {
  Future<String> calculation = Future<String>.delayed(Duration(seconds: 6), () {
    return 'Data Loaded';
  });
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('FutureBuilder Widget'),
      ),
      body: FutureBuilder(
        future: calculation,
        builder: (context, AsyncSnapshot snapshot) {
          List<Widget> children;
          if (snapshot.hasData) {
            children = [
              Icon(
                Icons.check_circle_outline,
                size: 200,
                color: Colors.green,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  'Result: ${snapshot.data}',
                  textScaleFactor: 2,
                ),
              ),
            ];
          } else {
            children = [
              SizedBox(
                width: 60,
                height: 60,
                child: CircularProgressIndicator(
                  backgroundColor: Colors.pink,
                  strokeWidth: 5,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 16),
                child: Text(
                  'Awaiting result...',
                  textScaleFactor: 2,
                ),
              )
            ];
          }
          return Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: children,
            ),
          );
        },
      ),
    );
  }
}
