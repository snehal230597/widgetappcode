import 'package:flutter/material.dart';

class FormFieldScreen extends StatefulWidget {
  @override
  _FormScreenState createState() => _FormScreenState();
}

class _FormScreenState extends State<FormFieldScreen> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Form Widget'),
      ),
      body: Form(
        key: formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            TextFormField(
              decoration: InputDecoration(hintText: 'Enter Email ID'),
               validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please Enter Some Text';
                }
                return null;
              },
            ),
            Padding(
              padding: const EdgeInsets.all(20),
              child: ElevatedButton(
                  onPressed: () {
                    if (formKey.currentState.validate()) {}
                  },
                  child: Text('Click Me!!!')),
            )
          ],
        ),
      ),
    );
  }
}
