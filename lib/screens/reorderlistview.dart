import 'package:flutter/material.dart';

class ReorderList extends StatefulWidget {
  @override
  _ReorderListState createState() => _ReorderListState();
}

class _ReorderListState extends State<ReorderList> {
  final List<int> items = List<int>.generate(50, (int index) => index);
  @override
  Widget build(BuildContext context) {
    ColorScheme colorScheme = Theme.of(context).colorScheme;
    Color oddItemColor = colorScheme.primary.withOpacity(0.05);
    Color evenItemColor = colorScheme.primary.withOpacity(0.15);
    return Scaffold(
      appBar: AppBar(
        title: Text('ReOrdered ListView'),
      ),
      body: ReorderableListView(
        children: [
          for (int index = 0; index < items.length; index++)
            ListTile(
              key: Key('$index'),
              tileColor: items[index].isOdd ? oddItemColor : evenItemColor,
              title: Text('Item ${items[index]}'),
            ),
        ],
        header: Text('This Is the Header!!!'),
        onReorder: (int oldIndex, int newIndex) {
          setState(
            () {
              if (oldIndex < newIndex) {
                newIndex -= 1;
              }
              final int item = items.removeAt(oldIndex);
              items.insert(newIndex, item);
            },
          );
        },
      ),
    );
  }
}
