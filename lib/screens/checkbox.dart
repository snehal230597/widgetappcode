import 'package:flutter/material.dart';

class CheckBoxScreen extends StatefulWidget {
  @override
  _CheckBoxScreenState createState() => _CheckBoxScreenState();
}

class _CheckBoxScreenState extends State<CheckBoxScreen> {
  bool isChecked = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('CheckBox Widget'),
      ),
      body: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Please Tapped This Button!',
              style: TextStyle(color: Colors.green, fontSize: 15),
            ),
            Checkbox(
              value: isChecked,
              activeColor: Colors.red,
              checkColor: Colors.white,
              onChanged: (bool value) {
                setState(
                  () {
                    isChecked = value;
                  },
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
