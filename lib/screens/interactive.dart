import 'package:flutter/material.dart';

class InteractiveScreen extends StatefulWidget {
  @override
  _InteractiveScreenState createState() => _InteractiveScreenState();
}

class _InteractiveScreenState extends State<InteractiveScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Interactive Viewer'),
      ),
      body: InteractiveViewer(
        boundaryMargin: EdgeInsets.all(50),
        minScale: 0.1,
        maxScale: 1.6,
        child: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [Colors.red, Colors.yellow],
            ),
          ),
        ),
      ),
    );
  }
}
