import 'package:flutter/material.dart';

class ThemePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        // brightness: Brightness.dark,
        colorScheme: ColorScheme.fromSwatch(
          primarySwatch: Colors.green,
        ).copyWith(
          secondary: Colors.green,
        ),
        textTheme: const TextTheme(bodyText2: TextStyle(color: Colors.purple)),
      ),
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text(
            'Theme',
            style:
                TextStyle(color: Colors.black87, fontWeight: FontWeight.bold),
          ),
          backgroundColor: Colors.green,
          // backgroundColor: Colors.amber,
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          // backgroundColor: Colors.orange,
          onPressed: () {},
        ),
        body: Column(
          children: [
            Center(
              child: FlutterLogo(
                size: 350,
                style: FlutterLogoStyle.horizontal,
                // textColor: Colors.white,
              ),
            ),
            Text(
              'Hello Snehal! How Are You?',
              textScaleFactor: 2,
            ),
            IconButton(
                icon: Icon(Icons.mail), color: Colors.orange, onPressed: () {}),
          ],
        ),
      ),
    );
  }
}
