import 'package:flutter/material.dart';

class InkwellNew2Screen extends StatefulWidget {
  @override
  _InkwellNew2ScreenState createState() => _InkwellNew2ScreenState();
}

class _InkwellNew2ScreenState extends State<InkwellNew2Screen> {
  int count = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('InkWell'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            InkWell(
              splashColor: Colors.yellow,
              borderRadius: BorderRadius.circular(10),
              highlightColor: Colors.pink,
              child: Icon(
                Icons.add_circle,
                size: 100,
              ),
              onDoubleTap: () {
                setState(() {
                  debugPrint('Double tappes');
                  count += 1;
                });
              },
              // onTap: () {
              //   setState(() {
              //     debugPrint('Single tappes');
              //     count += 1;
              //   });
              // },
              onTapDown: (value) {
                setState(() {
                  debugPrint('OnTapDown tappes');
                  count += 1;
                });
              },
              onLongPress: () {
                setState(() {
                  debugPrint('LongPress tappes');
                  count += 1;
                });
              },
            ),
          ],
        ),
      ),
    );
  }
}
