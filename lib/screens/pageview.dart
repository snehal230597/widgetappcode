import 'package:flutter/material.dart';

class PageviewScreen extends StatefulWidget {
  @override
  _PageviewScreenState createState() => _PageviewScreenState();
}

class _PageviewScreenState extends State<PageviewScreen> {
  @override
  Widget build(BuildContext context) {
    PageController controller = PageController();
    return Scaffold(
      appBar: AppBar(
        title: Text('PageView Screens'),
      ),
      body: PageView(
        controller: controller,
        scrollDirection: Axis.horizontal,
        children: [
          Center(
            child: Container(
                height: 50,
                width: 150,
                color: Colors.orange,
                alignment: Alignment.center,
                child: Text('First page')),
          ),
          Center(
            child: Container(
                height: 50,
                width: 150,
                color: Colors.red,
                alignment: Alignment.center,
                child: Text('Second page')),
          ),
          Center(
            child: Container(
                height: 50,
                width: 150,
                color: Colors.green,
                alignment: Alignment.center,
                child: Text('Third page')),
          ),
        ],
      ),
    );
  }
}
