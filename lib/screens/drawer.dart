import 'package:flutter/material.dart';

class DrawerScreen extends StatefulWidget {
  @override
  _DrawerScreenState createState() => _DrawerScreenState();
}

class _DrawerScreenState extends State<DrawerScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Drawer Demo'),
        backgroundColor: Colors.brown,
      ),
      drawer: Drawer(
        elevation: 5,
        child: ListView(
          children: [
            DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.green,
              ),
              child: Text(
                'Drawer Header',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
            ),
            ListTile(
              leading: Icon(
                Icons.message,
                color: Colors.green,
              ),
              title: Text('Message'),
              subtitle: Text('check massage'),
              trailing: Icon(Icons.ac_unit),
            ),
            ListTile(
              leading: Icon(
                Icons.account_circle,
                color: Colors.green,
              ),
              title: Text('Profile'),
              subtitle: Text('check your profile'),
            ),
            ListTile(
              leading: Icon(
                Icons.settings,
                color: Colors.green,
              ),
              title: Text('Settings'),
              subtitle: Text('please setting your phonne'),
            ),
            SizedBox(
              height: 100,
            ),
            RaisedButton(
              elevation: 10,
              child: Text('Close drawer'),
              color: Colors.green,
              onPressed: () {
                setState(
                  () {
                    Navigator.pop(context);
                  },
                );
              },
            )
          ],
        ),
      ),
    );
  }
}
