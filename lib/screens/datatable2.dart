import 'package:flutter/material.dart';

class DataTable2Screen extends StatefulWidget {
  @override
  _DataTable2ScreenState createState() => _DataTable2ScreenState();
}

class _DataTable2ScreenState extends State<DataTable2Screen> {
  static int numItems = 10;
  List<bool> selected = List<bool>.generate(numItems, (int index) => false);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Data Table 2'),
      ),
      body: ListView(
        children: [
          SizedBox(
            width: double.infinity,
            child: DataTable(
              columns: [DataColumn(label: Text('Number'))],
              rows: List<DataRow>.generate(
                numItems,
                (int index) => DataRow(
                  cells: [
                    DataCell(
                      Text('Row $index'),
                    )
                  ],
                  selected: selected[index],
                  onSelectChanged: (value) {
                    setState(() {
                      selected[index] = value;
                    });
                  },
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
