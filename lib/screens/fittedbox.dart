import 'package:flutter/material.dart';

class FittedBoxScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('FittedBox'),
      ),
      body: ListView(
        children: [
          SizedBox(
            height: 20,
          ),
          Center(
            child: Container(
              height: 400,
              width: 300,
              color: Colors.red,
              child: FittedBox(fit: BoxFit.cover, child: FlutterLogo()),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          SizedBox(
            height: 50,
            width: 200,
            child: Card(
                elevation: 15,
                color: Colors.lightBlue,
                child: Center(child: Text('Hello World!!!'))),
          ),
        ],
      ),
    );
  }
}
