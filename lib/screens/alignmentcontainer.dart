import 'package:flutter/material.dart';

class AlignmentContainerScreen extends StatefulWidget {
  @override
  _AlignmentContainerScreenState createState() =>
      _AlignmentContainerScreenState();
}

class _AlignmentContainerScreenState extends State<AlignmentContainerScreen> {
  bool selected = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          setState(() {
            selected = !selected;
          });
        },
        child: Center(
          child: AnimatedContainer(
            height: selected ? 100 : 200,
            width: selected ? 200 : 100,
            duration: Duration(seconds: 1),
            color: selected ? Colors.red : Colors.lime,
            alignment:
                selected ? Alignment.center : AlignmentDirectional.topCenter,
            curve: Curves.fastOutSlowIn,
            child: FlutterLogo(
              size: 50,
            ),
          ),
        ),
      ),
    );
  }
}
