import 'package:flutter/material.dart';

class Lob extends StatefulWidget {
  @override
  _LobState createState() => _LobState();
}

class _LobState extends State<Lob> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
          if (constraints.maxWidth < 500) {
            return Center(
              child: Container(
                height: constraints.maxHeight < 800
                    ? constraints.maxHeight * 0.5
                    : constraints.maxHeight,
                width: constraints.maxWidth,
                color: Colors.green,
              ),
            );
          } else {
            return Center(
              child: Container(
                height: constraints.maxHeight * 0.5,
                width: constraints.maxWidth * 0.5,
                color: Colors.red,
              ),
            );
          }
        },
      ),
    );
  }
}
