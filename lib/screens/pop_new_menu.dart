import 'package:flutter/material.dart';

class PopupMenuScreen extends StatefulWidget {
  @override
  _PopupMenuScreenState createState() => _PopupMenuScreenState();
}

class _PopupMenuScreenState extends State<PopupMenuScreen> {
  String value = '';
  List<int> list = [100, 200, 300, 400, 500];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('PopupMenu'), actions: [
        PopupMenuButton(
            color: Colors.yellow,
            elevation: 10,
            tooltip: 'This is popupmenu items',
            itemBuilder: (context) {
              return list.map((choise) {
                return PopupMenuItem(
                  value: choise,
                  child: Text("$choise"),
                );
              }).toList();
            }),
      ]),
      body: Center(
          child: Text(
        'Tap on the Three dots',
        textScaleFactor: 2,
      )),
    );
  }
}
