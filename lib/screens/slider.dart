import 'package:flutter/material.dart';

class SliderWidget extends StatefulWidget {
  @override
  _SliderWidgetState createState() => _SliderWidgetState();
}

class _SliderWidgetState extends State<SliderWidget> {
  double currentSliderValue = 20;
  bool isSwitched = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Slider Widget'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Slider Example',
              textScaleFactor: 1.5,
            ),
            SizedBox(height: 20),
            Slider(
                value: currentSliderValue,
                divisions: 5,
                min: 00,
                max: 100,
                activeColor: Colors.pink,
                label: currentSliderValue.round().toString(),
                onChanged: (double value) {
                  setState(() {
                    currentSliderValue = value;
                  });
                }),
            SizedBox(height: 20),
            Text(
              'Switch Button Example',
              textScaleFactor: 1.5,
            ),
            SizedBox(height: 20),
            Switch(
                value: isSwitched,
                activeColor: Colors.pink,
                onChanged: (value) {
                  setState(() {
                    isSwitched = value;
                  });
                })
          ],
        ),
      ),
      backgroundColor: Colors.lime,
    );
  }
}
