import 'package:flutter/material.dart';

class TransformationScreen extends StatefulWidget {
  @override
  _TransformationScreenState createState() => _TransformationScreenState();
}

class _TransformationScreenState extends State<TransformationScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Transformation Widget'),
      ),
      body: Center(
        child: Container(
          color: Colors.black,
          child: Transform(
            alignment: Alignment.topRight,
            transform: Matrix4.skewY(0.0)..rotateZ(90),
            child: Container(
              padding: EdgeInsets.all(10),
              color: Colors.lime,
              child: Text('ApartMent For Rent'),
            ),
          ),
        ),
      ),
    );
  }
}
