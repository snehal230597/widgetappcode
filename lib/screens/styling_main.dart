import 'package:flutter/material.dart';
import 'package:widgets_app/screens/styling_size.dart';
import 'package:widgets_app/screens/styling_orientation.dart';

class StylingTwo extends StatefulWidget {
  @override
  _StylingTwoState createState() => _StylingTwoState();
}

class _StylingTwoState extends State<StylingTwo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Media Query',
          style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
        ),
        backgroundColor: Colors.red,
      ),
      drawer: Drawer(),
      body: ListView(
        padding: EdgeInsets.only(top: 20, left: 60, right: 60),
        children: [
          ElevatedButton(
            child: Text(
              'Size',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => Styling(),
                ),
              );
            },
          ),
          ElevatedButton(
            child: Text(
              'Orientation',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => Orient(),
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
