import 'package:flutter/material.dart';

class AnimatedAlignScreeen extends StatefulWidget {
  @override
  _AnimatedAlignScreeenState createState() => _AnimatedAlignScreeenState();
}

class _AnimatedAlignScreeenState extends State<AnimatedAlignScreeen> {
  bool selected = true;
  bool first = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text(
              'AnimatedAlign',
              textScaleFactor: 3,
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  selected = !selected;
                });
              },
              child: Container(
                height: 250,
                width: 250,
                color: Colors.red,
                child: AnimatedAlign(
                  alignment:
                      selected ? Alignment.topRight : Alignment.bottomLeft,
                  duration: Duration(seconds: 2),
                  curve: Curves.fastOutSlowIn,
                  child: FlutterLogo(
                    size: 80,
                  ),
                ),
              ),
            ),
            ElevatedButton(
              onPressed: () {
                setState(() {
                  first = !first;
                });
              },
              child: Text('AnimatedCrossFade'),
            ),
            AnimatedCrossFade(
                firstChild: FlutterLogo(
                  style: FlutterLogoStyle.horizontal,
                  size: 100,
                ),
                secondChild: FlutterLogo(
                  style: FlutterLogoStyle.markOnly,
                  size: 100,
                ),
                crossFadeState: first
                    ? CrossFadeState.showFirst
                    : CrossFadeState.showSecond,
                duration: Duration(seconds: 2))
          ],
        ),
      ),
    );
  }
}
