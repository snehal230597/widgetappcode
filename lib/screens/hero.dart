import 'package:flutter/material.dart';
import 'package:widgets_app/home_screen.dart';

class HeroScreen extends StatefulWidget {
  @override
  _HeroScreenState createState() => _HeroScreenState();
}

class _HeroScreenState extends State<HeroScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Hero Widget first Page'),
      ),
      body: Column(
        children: [
          SizedBox(
            height: 200,
          ),
          ListTile(
            leading: Hero(
              tag: 'hello',
              child: Container(
                height: 50,
                width: 50,
                color: Colors.green,
              ),
            ),
            onTap: () => _goToSecondPage(context),
            title: Text('Tap on the icon to view hero animation transition'),
          ),
          SizedBox(
            height: 20,
          ),
          ElevatedButton(
              onPressed: () {
                setState(
                  () {
                    Navigator.pop(
                      context,
                      MaterialPageRoute(
                        builder: (BuildContext context) => HomeScreen(),
                      ),
                    );
                  },
                );
              },
              child: Text('Cliked For BackScreen'))
        ],
      ),
    );
  }
}

void _goToSecondPage(BuildContext context) {
  Navigator.of(context).push(
    MaterialPageRoute(
      builder: (BuildContext context) {
        return Scaffold(
          appBar: AppBar(
            title: Text('Hero Widget Second Page'),
            backgroundColor: Colors.orange,
          ),
          body: Padding(
            padding: const EdgeInsets.all(20),
            child: Container(
              height: 200,
              width: 200,
              color: Colors.green,
            ),
          ),
        );
      },
    ),
  );
}
