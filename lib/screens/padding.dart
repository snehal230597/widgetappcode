import 'package:flutter/material.dart';

class PagePadding extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Padding'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(25),
        child: Column(
          children: [
            Container(
              height: 100,
              width: 300,
              child: Card(
                child: Padding(
                  padding: const EdgeInsets.only(left: 85, top: 30),
                  child: Text(
                    'Hello World',
                    textScaleFactor: 1.5,
                  ),
                ),
                color: Colors.lime,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(5),
              child: Container(
                height: 100,
                width: 300,
                color: Colors.red,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(5.0),
              child: Container(
                height: 100,
                width: 300,
                color: Colors.pink,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(5.0),
              child: Container(
                height: 100,
                width: 300,
                color: Colors.green,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
