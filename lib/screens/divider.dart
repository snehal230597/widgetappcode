import 'package:flutter/material.dart';

class DividerScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Divider'),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            children: [
              Expanded(
                child: Container(
                  color: Colors.red,
                  child: Center(child: Text('Above')),
                ),
              ),
              Divider(
                color: Colors.black,
                height: 25,
                thickness: 4,
                endIndent: 2,
                indent: 2,
              ),
              Expanded(
                child: Container(
                  color: Colors.yellow,
                  child: Center(child: Text('Below')),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
