import 'package:flutter/material.dart';
import 'package:widgets_app/home_screen.dart';

void main() {
  runApp(
    MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Widgets App',
      home: HomeScreen(),
    ),
  );
}
